require 'rack/test'
require_relative '../app/main'

RSpec.describe App::Main do
  include Rack::Test::Methods

  let(:app){ App::Main }

  before do
    allow_any_instance_of(App::WeatherFetcher).to receive(:response).
      with([ 3444924, 2735943, 3451190, 5128581, 3452925 ]){
        JSON.unparse(
          list: [
            { main: { temp: 29 }, id: 3444924 },
            { main: { temp: 30 }, id: 2735943 },
            { main: { temp: 28 }, id: 3451190 },
            { main: { temp: 25 }, id: 5128581 },
            { main: { temp: 20 }, id: 3452925 },
          ]
        )
      }
  end

  example 'find the best weather (the one of the highest temperature)' do
    get '/'
    expect(last_response.ok?).to be true
    expect(last_response.body).to include 'Porto (30)'
  end
end
