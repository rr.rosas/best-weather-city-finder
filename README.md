# Best weather city finder

This applications will display the city with the highest temperature from a fixed list of cities
(Vitória, Porto, Rio de Janeiro, Porto Alegre and New York) based on data from
[OpenWeatherMap](http://openweathermap.org).

# TODO

- implement a caching mechanism to store the latest temperature for at least the last 10 minutes,
since they don't change often.

# Running the application

    bin/puma -p 9292

Just access the application navigating to http://localhost:9292/.

It will display the city with the highest temperature as well as the temperature itself.
