if ENV['RACK_ENV'] != 'development'
  require_relative 'app/main'
  run App::Main
else
  require 'auto_reloader'
  AutoReloader.activate reloadable_paths: [File.join(__dir__, 'app')], delay: 1
  run ->(env) {
    AutoReloader.reload! do
      require_relative 'app/main'
      App::Main.call env
    end
  }
end
