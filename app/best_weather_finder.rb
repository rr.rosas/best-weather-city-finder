# frozen_string_literal: true

require_relative 'weather_fetcher'

module App
  class BestWeatherFinder
    def self.best_weather
      WeatherFetcher.fetch(WeatherFetcher::CITIES.keys).
        sort_by{|weather| -weather.temperature }.first
    end
  end
end
