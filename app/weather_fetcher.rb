# frozen_string_literal: true

require 'json'
require 'net/http'

module App
  class WeatherFetcher
    KEY = '60a360e42c6dfe935ba6d28732a8a75a'
    SERVICE_URL = 'http://api.openweathermap.org/data/2.5/group'
    CITIES = {
      3444924 => 'Vitória',
      2735943 => 'Porto',
      3451190 => 'Rio de Janeiro',
      5128581 => 'New York',
      3452925 => 'Porto Alegre',
    }

    def self.fetch(city_ids)
      new(city_ids).fetch
    end

    def initialize(city_ids)
      @city_ids = city_ids
    end

    def fetch
      JSON.parse(response(@city_ids))['list'].map do |r|
        Weather.new CITIES[r['id']], r['main']['temp']
      end
    end

    private

    def response(city_ids)
      uri = URI(SERVICE_URL)
      params = { APPID: KEY, id: city_ids.join(','), units: 'metric' }
      uri.query = URI.encode_www_form params
      Net::HTTP.get_response(uri).body
    end

    class Weather
      attr_reader :city_name, :temperature
      def initialize(city_name, temperature)
        @city_name, @temperature = city_name, temperature
      end
    end
  end
end
