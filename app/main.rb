# frozen_string_literal: true

require 'sinatra/base'
require_relative 'best_weather_finder'

module App
  class Main < Sinatra::Base
    get '/' do
      best_weather = BestWeatherFinder.best_weather
      erb :index, locals: { best_weather: best_weather }
    end
  end
end
